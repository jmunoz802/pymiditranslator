import win32com.client
import launchpad_py as lppy
import time
import threading
from WAV import *
from KeyboardSim import *
from Buttons import *

def PressKeys(*keys):
    for key in keys[0]:
        SendInput(Keyboard(key))

    time.sleep(0.05)
    
    for key in keys[0]:
        SendInput(Keyboard(key, KEYEVENTF_KEYUP))

class MidiTranslator:
    def __init__(self):
        self.lp = lppy.Launchpad()
        self.lp.Open(0, 'Launchpad Mini')
        self.lp.ListAll()
        self.buttons = [( (0,8), SinglePressButton( self.lp, (0,8), [0, 2], PressKeys, VK_LCONTROL, VK_NUMPAD7)),
                        ( (2,8), SinglePressButton( self.lp, (2,8), [0, 2], PressKeys, VK_LCONTROL, VK_NUMPAD5)),
                        ( (0,7), SinglePressButton( self.lp, (0,7), [0, 2], PressKeys, VK_LCONTROL, VK_NUMPAD6)),
                        ( (1,8), SinglePressButton( self.lp, (1,8), [0, 2], PressKeys, VK_LCONTROL, VK_NUMPAD8)),
                        ( (3,8), SinglePressButton( self.lp, (3,8), [0, 2], PressKeys, VK_LCONTROL, VK_NUMPAD9)),
                        ( (3,7), SinglePressButton( self.lp, (3,7), [0, 2], PressKeys, VK_LCONTROL, VK_NUMPAD4)),
                        ( (7,8), SingleSoundBite( self.lp, 'Oh My', 'ohmy.wav', (7,8), [0, 2])),
                        ( (4,8), SingleSoundBite( self.lp, 'Rimshot', 'rimshot.wav', (4,8), [0, 2])),
                        ( (5,8), SingleSoundBite( self.lp, 'Trombone', 'Sad Trombone.wav', (5,8), [0, 2])),
                        ( (6,8), HeldSoundBite( self.lp, 'AirHorn', 'AirHorn.wav', (6,8), [0, 2])),
                        ( (4,7), SingleSoundBite( self.lp, 'YesRoundabout', 'YesRoundabout.wav', (4,7), [0, 2])),
                        ( (5,7), SingleSoundBite( self.lp, 'DunDunDun', 'dundundun.wav', (5,7), [0, 2]))]
        #print(self.buttons)
        self.lp.LedCtrlXY(0, 0, 2, 0)
        
    def poll(self):
        while True:
            state = self.lp.ButtonStateXY()
            #print(state)
            #for state in states:
            if len(state) > 0:
                tup = (state[0], state[1])

                if(tup == (0,0)):
                    break
                
                buttonList = [x[1] for x in self.buttons if x[0] == tup]
                button = buttonList[0] if len(buttonList) > 0 else False
                #print(button)
                if button:
                    if state[2]:
                        button.buttonPress(self.lp)
                    else:
                        button.buttonRelease(self.lp)
                    
            time.sleep(0.05)
        print("Stopped Polling!")

m = MidiTranslator()

m.poll()

m.lp.Reset()
m.lp.Close()

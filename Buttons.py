class MidiButton:
    def __init__(self, xy, color, func, *args):
        self.xy = xy
        self.func = func
        self.funcArgs = args[0]
        #print(self.funcArgs)
        self.color = color
        pass
    def buttonPress(self, lp):
        raise NameError("Func: buttonPress, wasn't overwritten")
        pass
    def buttonRelease(self, lp):
        raise NameError("Func: buttonRelease, wasn't overwritten")
        pass

class SinglePressButton(MidiButton):
    def __init__(self, lp, xy, color, func, *args):
        super().__init__(xy, color, func, args)
        lp.LedCtrlXY(self.xy[0], self.xy[1], 1, 0)
        
    def buttonPress(self, lp):
        lp.LedCtrlXY(self.xy[0], self.xy[1], self.color[0], self.color[1])
        if self.func:
            self.func(self.funcArgs)

    def buttonRelease(self, lp):
        lp.LedCtrlXY(self.xy[0], self.xy[1], 1, 0)
        
class ToggleButton(MidiButton):
    def __init__(self, lp, xy, color, func, *args):
        super().__init__(xy, color, func, args)
        self.on = false
        lp.LedCtrlXY(self.xy[0], self.xy[1], 2, 0)
        
    def buttonPress(self, lp):
        if self.on:
            lp.LedCtrlXY(self.xy[0], self.xy[1], self.color[0], self.color[1])
            if self.func:
                self.func(True, self.funcArgs)
        else: 
            lp.LedCtrlXY(self.xy[0], self.xy[1], 2, 0)
            if self.func:
                self.func(False, self.funcArgs)
        self.on = not self.on
    def buttonRelease(self, lp):
        pass

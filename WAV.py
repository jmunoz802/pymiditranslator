import pyaudio
import wave
import sys
import threading 
from Buttons import *

def PlayWav(filename):
  CHUNK = 1024

  if filename is None:
      #print("Plays a wave file.\n\nUsage: %s filename.wav" % filename)
      sys.exit()
      return

  wf = wave.open(filename, 'rb')

  # instantiate PyAudio (1)
  p = pyaudio.PyAudio()

  # open stream (2)
  stream = p.open(format=p.get_format_from_width(wf.getsampwidth()),
                  channels=wf.getnchannels(),
                  rate=wf.getframerate(),
                  output=True)

  # read data
  data = wf.readframes(CHUNK)

  # play stream (3)
  while len(data) > 0:
      stream.write(data)
      data = wf.readframes(CHUNK)

  # stop stream (4)
  stream.stop_stream()
  stream.close()

  # close PyAudio (5)
  p.terminate()
  sys.exit()
#End of PlayWav(filename)

class AudioBite(threading.Thread):
  def __init__(self, name, filename):
    super().__init__()
    self.filename = filename
    self.running = True

  def stopRun(self):
      self.running = False

  def run(self):
      CHUNK = 1024

      if self.filename is None:
          #print("Plays a wave file.\n\nUsage: %s filename.wav" % filename)
          sys.exit()
          return

      wf = wave.open(self.filename, 'rb')

      # instantiate PyAudio (1)
      p = pyaudio.PyAudio()

      # open stream (2)
      stream = p.open(format=p.get_format_from_width(wf.getsampwidth()),
                      channels=wf.getnchannels(),
                      rate=wf.getframerate(),
                      output=True)

      # read data
      data = wf.readframes(CHUNK)
  
      # play stream (3)
      while len(data) > 0 and self.running:
          stream.write(data)
          data = wf.readframes(CHUNK)

      # stop stream (4)
      stream.stop_stream()
      stream.close()

      # close PyAudio (5)
      p.terminate()
      sys.exit()

class SingleSoundBite(SinglePressButton):
  def __init__(self, lp, name, filename, xy, color):
    super().__init__(lp, xy, color, None, None)
    self.name = name
    self.filename = filename
    self.audiobite = AudioBite(name, filename)
    
  def buttonPress(self, lp):
      super().buttonPress(lp)
      if not self.audiobite.is_alive():
          self.audiobite.start()
          self.audiobite = AudioBite(self.name, self.filename)
      pass
  def buttonRelease(self, lp):
      super().buttonRelease(lp)
      pass

class HeldSoundBite(SinglePressButton):
  def __init__(self, lp, name, filename, xy, color):
    super().__init__(lp, xy, color, None, None)
    self.name = name
    self.filename = filename
    self.audiobite = AudioBite(name, filename)
    
  def buttonPress(self, lp):
      super().buttonPress(lp)
      if not self.audiobite.is_alive():
          self.audiobite.start()
      pass
  def buttonRelease(self, lp):
      super().buttonRelease(lp)
      self.audiobite.stopRun()
      self.audiobite = AudioBite(self.name, self.filename)
      pass

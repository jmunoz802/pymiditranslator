PyMidiTranslator

This small python script is intended to be used in conjunction with a device that sends midi messages on the PC. The script reads in MIDI messages and then is open to translating them to keyboard strokes or be used in other programmatical ways. 

In my user case, I use a Novation Launchpad MINI that translates to keystrokesor makes sound like a soundboard.

Improvements/Ideas
 - Use of a Configeration file for setup of buttons/colors/functions/classes
 - Test the use of Toggle Button

Dependencies
 - KeyboardSim.py - keypresses library for Win
 - launchpad.py - reading MIDI msgs, and color setup for Launchpad hardware
